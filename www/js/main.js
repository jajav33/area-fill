//ToolBar members
var numOfSmall = 1;
var incrmntVal = 1;
var minVal = 1;
var maxVal = 10;
var btnStart = document.getElementById('btnStart');
var btnDecNum = document.getElementById('btnDecNum');
var btnIncNum = document.getElementById('btnIncNum');
var inRadius = document.getElementById('userRadiusText');

//Canvas members
var canvas = document.getElementById('mainCanvas');
var ctx = canvas.getContext('2d');

var SmallCircles = new Array();
var PuddleCircles = new Array();
var sSelected = false;


//ToolBar functions
btnIncNum.addEventListener('click', function () {
	numOfSmall = numOfSmall + 1;
	if(numOfSmall > maxVal){
		numOfSmall = maxVal;	
	}
	
	var txt = document.createTextNode(numOfSmall);
	inRadius.innerText = txt.textContent;
});

btnDecNum.addEventListener('click', function () {
	numOfSmall = numOfSmall - 1;
	if(numOfSmall < minVal){
		numOfSmall = minVal;	
	}
	
	var txt = document.createTextNode(numOfSmall);
	inRadius.innerText = txt.textContent;
});

btnStart.addEventListener('click', function () {
	if(numOfSmall < minVal){
		numOfSmall = minVal;	
	}
	else if(numOfSmall > maxVal){
		numOfSmall = maxVal;	
	}
	
	var txt = document.createTextNode(numOfSmall);
	inRadius.innerText = txt.textContent;
	
	init();
});





//Game objects
var MouseObj = {x: 0, y: 0, radius: 0, state: 'mouseup',
					setState: function (newState) { this.state = newState; } 
};

var MainCircle = 	{	x: canvas.width/4,
							y: 3*(canvas.height/4),
							radius: 300 ,
							A: 0,
							draw: function () {								
								ctx.beginPath();
								ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2, false);
								
								ctx.lineWidth = 0.5;
								ctx.strokeStyle = 'black';
								ctx.stroke();
							}
};
						
var SmallCircle = function (){ 
	this.x = 0; 
	this.y= 0; 
	this.radius = 1; 
	this.A = 0;
	this.isSelected = false;
	this.selected = function (bool) {
		if( sSelected == false){
			this.isSelected = bool;		
			sSelected = bool;	
		}			
	}
	this.update = function () {
		if( this.isSelected == true ){
			this.x = MouseObj.x;
			this.y = MouseObj.y;		
		}	
		
	}
	this.draw = function () {
		ctx.beginPath();
		ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2, false);

		ctx.fillStyle = 'black';
		ctx.fill();
	}
}

var PuddleCircle = function (p_index, p_currA) {
	this.x = canvas.width/4,
	this.y = canvas.height/2, 
	this.index = p_index;
	this.outRad = 0;
	this.inRad = 0; 
	this.currA = p_currA;
	this.outA = 0;
	this.inA = 0;

	this.update = function () {

	}

	this.draw = function () {
		//Change color depending on Index value.
		var vRgba = new Array();		
		vRgba.push(255,255,255,1.0);
		var colorIndx = this.index % 3;
		//using modulo allows me to not worry restarting when reaching zero.		
		var newVal = (this.index*10)%255;
		vRgba[colorIndx] = newVal;
		
		//Calculate outer and inner radius using SmallCircles Area
		var calcRad = (this.index +1)*this.currA/Math.PI;
		calcRad = Math.sqrt(calcRad);
		this.outRad = calcRad;
		
		calcRad = (this.index)*this.currA/Math.PI;
		calcRad = Math.sqrt(calcRad);
		this.inRad = calcRad;
		
		var radius = this.outRad;
		for( radius = this.outRad; radius > this.inRad; radius = radius - 0.1){
			ctx.beginPath();
			ctx.arc(this.x, this.y, radius, 0, Math.PI*2, false);
			
			var colorString = 'rgba('+vRgba[0]+','+vRgba[1]+','+vRgba[2]+','+vRgba[3]+')';
			ctx.strokeStyle = colorString; 
			ctx.stroke();
		} 
		
	}

} 			



//misc
function collisionCheck(obj1, obj2) {
	var totalRadius = 0;
	var totalDist = 0;
	var result = false;
		
	totalRadius = obj1.radius + obj2.radius;
	totalDist = ((obj1.x - obj2.x) * (obj1.x - obj2.x)) + ((obj1.y - obj2.y) * (obj1.y - obj2.y)); 
	totalDist = Math.sqrt(totalDist);	
	
	result = (totalDist <= totalRadius);
	
	return result;
}



//event functions
canvas.addEventListener('mousedown', function () {
	if( event.buttons !== 1 ){
		return;	
	}	
	MouseObj.setState('mousedown');
});
canvas.addEventListener('touchstart', function () {
	event.preventDefault();
	
	MouseObj.setState('mousedown');
	
});

canvas.addEventListener('mouseup', function () {
	MouseObj.setState('mouseup');
});

canvas.addEventListener('touchend', function () {
	event.preventDefault();
	
	MouseObj.setState('mouseup');
	
});

canvas.addEventListener('mouseleave', function () {MouseObj.setState('mouseup');})	;
canvas.addEventListener('mousemove', function () { 
	var rect = canvas.getBoundingClientRect();
	
	MouseObj.x = event.clientX-rect.left; 
	MouseObj.y = event.clientY-rect.top;
});
canvas.addEventListener('touchmove', function () { 
	event.preventDefault();
	var rect = canvas.getBoundingClientRect();
	
	MouseObj.x = event.targetTouches[0].pageX; 
	MouseObj.y = event.targetTouches[0].pageY;
});
	
	
function circleSelect() {
	for (var i = 0; i < SmallCircles.length; i++){
		var result = collisionCheck(MouseObj,	SmallCircles[i]);
		SmallCircles[i].selected(result);
		
		if(result == true){
			break;		
		}
	}	
}

function circleDeselect() {
	var delIndex = -1;
	for (var i = 0; i < SmallCircles.length; i++){
		//check if selected circle collides with big circle
		if( collisionCheck(SmallCircles[i], MainCircle) ){
			//Animate circle being dropped
			var newPuddle = new PuddleCircle(PuddleCircles.length, SmallCircles[i].A);		
			PuddleCircles.push(newPuddle);			
			
			//track what circle to delete
			delIndex = i;
		}
		
		//deselect all circles		
		sSelected = false;
		SmallCircles[i].selected(false);
	}
	
	//remove the small circle that collided.
	if( delIndex >= 0 ){
		//if user changes num of small circles, but changes 
		//mind to continue with the prototype, value display is off.		
		//this fix adjusts numbers properly if collision occurs.
		numOfSmall = SmallCircles.length;		
		
		SmallCircles.splice(delIndex, 1);
		numOfSmall = numOfSmall -1;
		var txt = document.createTextNode(numOfSmall);
		inRadius.innerText = txt.textContent;
	
	}
	

}

function mouseMoving(){
	//MouseObj.x = ev.clientX; 
	//MouseObj.y = ev.clientY; 
	
}


//----Game Loop Functions----
//Init
function init() {
	//clear small Circles and puddle circle
	SmallCircles.splice(0);
	PuddleCircles.splice(0);	
	
	//1 calc big circle area
	MainCircle.A = Math.PI * MainCircle.radius * MainCircle.radius;

	if(numOfSmall != 0){
		//2 calc area of small circl (A/n)
		var newA = MainCircle.A/numOfSmall;
		//2 calc the radius of small circles
		var newRad = Math.sqrt(newA/Math.PI);
		//2 calc numOfRows
		var colX = Math.floor(canvas.width/(2*newRad));
		var gapX = Math.floor(canvas.width%(2*newRad))/colX;
		//2 calc starting posY
		var rowY = Math.ceil(numOfSmall/colX);
		//2 create the small cricles and add to array. (draw them as well)
		var count =0;
		for( var iY =0; iY < rowY; iY++){
			for( var iX = 0; iX < colX; iX++){
				//break if row col exceeds num of small circles			
				if( count >= numOfSmall){
					break;			
				}			
		
				//else draw the circles.
				var newSCircle = new SmallCircle();
				newSCircle.A = newA;
				newSCircle.radius = newRad;	
				newSCircle.x = iX*(canvas.width/colX)+newRad+(gapX/2);
				newSCircle.y = iY*(2*newRad)+newRad;	

				newSCircle.draw();
	
				SmallCircles.push(newSCircle);
				
				count++;
			}
		}
	}
	
	//3 Draw big circle
	var gap = 8;
	var bigY = rowY*(2*newRad) + MainCircle.radius + gap;
	var bigX = canvas.width/2;
	MainCircle.x = bigX;
	MainCircle.y = bigY;
	MainCircle.draw();
	
	
	//redraw canvas and div holding canvas
	canvas.height = rowY*(2*newRad) + (2*MainCircle.radius) + (2*gap);
	document.getElementById('mainCanvas-wrap').style.height = canvas.height +"px";
		
	
	// remove end scene
	var endScene = document.getElementById('endScene')
	endScene.style.display = 'none';	
	
	main();
}	//end init()

//update
function update() {
	//mouse events
	switch(MouseObj.state){
		case 'mouseup':
			circleDeselect();
			break;
		case 'mousedown':
			circleSelect();
			break;
	}
	
	//update circles
	for (var i = 0; i < SmallCircles.length; i++){
		SmallCircles[i].update();
	}
}	//end update()

//render
function render() {
	ctx.clearRect(0,0,canvas.width, canvas.height);
	
	//draw big circle
	MainCircle.draw();
	
	//draw puddle circles
	for(var i=0; i < PuddleCircles.length; i++){
		PuddleCircles[i].x = MainCircle.x;
		PuddleCircles[i].y = MainCircle.y;
		
		PuddleCircles[i].draw();
	}
	
	//draw small circles
	for (var i = 0; i < SmallCircles.length; i++){
		SmallCircles[i].draw();	
	}	
}	//end render()


function main() {
	//check if end of game
	if( SmallCircles.length == 0){
		endGame();
		return;
	}

	update();
	render();
	
	requestAnimationFrame(main);
}	//end main()

//end game
function endGame() {
	var endScene = document.getElementById('endScene')
	endScene.style.display = 'block';
}	//end endGame


//Start Game
init();